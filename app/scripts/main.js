$(document).ready(function() {

  $('.home-welcome').slick({
    dots: false,
    arrows: false,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/icon_owl_1_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/icon_owl_1_right.png"></button>',
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000,
  });

  $('.home-welcome').append('<div class="container"><button type="button" class="slick-prev"><img src="./images/icon_owl_1_left.png"></button><button type="button" class="slick-next"><img src="./images/icon_owl_1_right.png"></button></div>');

  $('.slick-prev-next').click(function() {
    $('.home-welcome').slick('slickPrev');
  });

  $('.slick-next').click(function() {
    $('.home-welcome').slick('slickNext');
  });

  $('.home-vip__slides').slick({
    dots: false,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/icon_owl_2_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/icon_owl_2_right.png"></button>',
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.home-room__slides').slick({
    dots: false,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/icon_owl_2_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/icon_owl_2_right.png"></button>',
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.another__slides').slick({
    dots: false,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/icon_owl_2_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/icon_owl_2_right.png"></button>',
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.home-reviews__slides').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="./images/icon_owl_2_left.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="./images/icon_owl_2_right.png"></button>',
    dots: true,
    infinite: true,
    speed: 300
  });

  $('.home-about__tab-switcher').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('.home-about__tab-switcher').removeClass('home-about__tab-switcher_active');
    $('.home-about__tab-content').removeClass('home-about__tab-content_active');

    $(this).addClass('home-about__tab-switcher_active');
    $('#' + tab_id).addClass('home-about__tab-content_active');
  });

});